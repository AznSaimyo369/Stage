USER=$1

ansible-playbook -v -u $USER -i inventory.ini start.yml --extra-vars "@vars.yml"

ansible-playbook -v -u $USER -i inventory.ini dotclear.yml --extra-vars "@vars_dotclear.yml"

ansible-playbook -v -u $USER -i inventory.ini blog.yml --extra-vars "@vars_blog.yml"

ansible-playbook -v -u $USER -i inventory.ini blog.yml --extra-vars "@vars_modop.yml"

ansible-playbook -v -u $USER -i inventory.ini blog.yml --extra-vars "@vars_jaga.yml"

ansible-playbook -v -u $USER -i inventory.ini admin.yml --extra-vars "@vars_admin.yml"
