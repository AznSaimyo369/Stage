#!/bin/sh

USER=$1
PASSWORD=$2
BDD_NAME=$3
NAME_SQL=spip_$BDD_NAME
PATH=$4
NAME_IMG=$5
NAME_AUTO=$6
NAME=$NAME_SQL_$NAME_IMG_$NAME_AUTO

mysqldump -u $USER -p$PASSWORD $BDD_NAME | gzip > $NAME_SQL.sql.gz

mv $NAME_SQL.sql.gz $PATH

cd $PATH

tar -czvf $NAME_IMG.tar.gz IMG

tar -czvf $NAME_AUTO.tar.gz plugins

tar -czvf $NAME.tar.gz $NAME_SQL.sql.gz $NAME_IMG.tar.gz $NAME_AUTO.tar.gz
