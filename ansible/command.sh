USER=$1


ansible-playbook -v -u $USER -i inventory.ini start.yml --extra-vars "@vars.yml"

ansible-playbook -v -u $USER -i inventory.ini spip.yml --extra-vars "@vars_fph.yml"

ansible-playbook -v -u $USER -i inventory.ini spip.yml --extra-vars "@vars_fiuc.yml"
